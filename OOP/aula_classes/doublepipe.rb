class Person
    attr_reader :age
    attr_accessor :name

    def initialize (name, age)
        @name = name
        self.age = age
    end

    def age= (new_age)
        @age || = 18 #se o argumento for aceito usa o @age se não o 18
        @age = new_age unless new_age > 120
    end
end

