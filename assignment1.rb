some_var = false
another_var = nil

case some_var
when "pink elephant"
    puts "Don't think about the pink elephant in the room"
when false
    puts "looks like this one should execute"
when another_var.nil?
    puts "question mark in the method name?"
else
    puts "I guess nothing matched.. why ?"
end