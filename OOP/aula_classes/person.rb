
class Person
    def initialize(name, age)
          @name = name
          @age = age
    end
    def get_info
        @additional_info = "Interesting"
        "Name: #{@name} , Age: #{@age}"
    end
end

person1 = Person.new("Joe", 22)
p person1.instance_variables
puts person1.get_info
p person1.instance_variables
