def fatorial (n = 4)
    n == 0? 1: n * fatorial(n-1)
end

puts fatorial 5
puts fatorial 