single_quoted = 'ice cream \n followed by it\'s a party!'
double_quoted = "ice cream \n followed by it\'s a party!"

puts single_quoted
puts double_quoted

def multiply(one, two)
    "#{one} multiplied by #{two} equals #{one * two}"
end 

puts multiply(5,20)

my_name = "       victoria"
puts my_name
p my_name.lstrip
p my_name.lstrip.capitalize
my_name.lstrip!
my_name[0] = 'K'
puts my_name

cur_weather = %Q{
    It's a hot day outside
    grab your umbrellas...
    :o
    >(
}

puts cur_weather

cur_weather.lines do |line|
    line.sub! 'hot', 'rainy'
    line.sub! ':o', 'vish'
    puts "#{line.strip}"
end