module MyModule
    MyConstant = 'Outer Constant'
    class MyClass
        puts MyConstant
        MyConstant = 'Inner Constant'
        puts MyConstant
    end
    puts MyConstant
end