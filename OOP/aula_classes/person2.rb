
class Person
    def initialize(name, age)
          @name = name
          @age = age
    end
    #getter
    def name
        @name
    end
    #setter
    def name= (new_name)
        @name = new_name
    end
end

person1 = Person.new("Joe", 21)
p person1.name
person1.name = "Mike"
p person1.name