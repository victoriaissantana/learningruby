class Person
    attr_reader :age #apenas getter
    attr_accessor :name #getter e setter

    #construtor
    def initialize (name, ageVar)
        @name = name
        self.age = ageVar #metodo age=
        puts age
    end

    def age= (new_age)
        @age = new_age unless new_age > 120
    end
end

person1 = Person.new("Kim", 23)
p "My age is #{person1.age}"
person1.age = 130
p person1.age